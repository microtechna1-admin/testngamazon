package base;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Base {
	public static WebDriver driver;
	public static Properties config;
	public static FileInputStream fic;
	@BeforeSuite
	public void setup() {
		config = new Properties();
		try {
			fic = new FileInputStream(System.getProperty("user.dir")+"\\src\\test\\resources\\properties\\Config.properties");
			config.load(fic);
		} 
		catch (IOException e) {
			System.out.println("system not generate");
		}
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
	}
	@AfterSuite
	public void tearDown() {
		driver.close();
	}
	public void get(String url) {
		driver.get(url);
	}
	
	

}
